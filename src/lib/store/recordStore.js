// src/recordStore.js
import { writable } from 'svelte/store';
import { completeTask } from './userStore.js';
const RECORDS_STORAGE_KEY = 'records';
export const isPopupOpen = writable(false);
function createRecordStore() {
	let storedRecords = [];
	if (typeof window !== 'undefined') {
		storedRecords = JSON.parse(localStorage.getItem(RECORDS_STORAGE_KEY)) || storedRecords;
	}

	const { subscribe, set, update } = writable(storedRecords);

	return {
		subscribe,
		set: (records) => {
			localStorage.setItem(RECORDS_STORAGE_KEY, JSON.stringify(records));
			set(records);
		},
		update: (fn) => {
			update((records) => {
				const newRecords = fn(records);
				localStorage.setItem(RECORDS_STORAGE_KEY, JSON.stringify(newRecords));
				return newRecords;
			});
		}
	};
}

export const records = createRecordStore();
export function addRecord({ title, content, type = 'user' }) {
	records.update((state) => [
		{
			id: Date.now(),
			title,
			content,
			type,
			date: new Date().toISOString()
		},
		...state
	]);
	completeTask(type === 'user' ? 10 : 1);
}
