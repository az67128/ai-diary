// src/userStore.js
import { writable } from 'svelte/store';

const USER_STORAGE_KEY = 'user';

function createUserStore() {
	let storedUser = { name: 'AZ67128', level: 1, experience: 0, avatarUrl: '/images/avatar/1.webp' };
	if (typeof window !== 'undefined') {
		storedUser = JSON.parse(localStorage.getItem(USER_STORAGE_KEY)) || storedUser;
	}

	const { subscribe, set, update } = writable(storedUser);

	return {
		subscribe,
		set: (user) => {
			localStorage.setItem(USER_STORAGE_KEY, JSON.stringify(user));
			set(user);
		},
		update: (fn) => {
			update((user) => {
				const newUser = fn(user);
				localStorage.setItem(USER_STORAGE_KEY, JSON.stringify(newUser));
				return newUser;
			});
		}
	};
}

export const user = createUserStore();
export function completeTask(score) {
	user.update((state) => {
		state.experience = state.experience + score;
		if (state.experience >= 100) {
			state.experience = state.experience - 100;
			state.level = state.level + 1;
		}
		return state;
	});
}
