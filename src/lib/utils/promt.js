export async function fetchDailyQuestion() {
	const res = await fetch('/api/question2');
	const json = await res.json();
	return json.result.content;
}

export async function fetchDailyAdvise(history) {
	const res = await fetch('/api/advise2', {
		method: 'post',
		headers: { 'Content-type': 'application/json' },
		body: JSON.stringify({history})
	});
	const json = await res.json();
	return json.result;
}
