import { json } from '@sveltejs/kit';
import { Configuration, OpenAIApi } from 'openai';

const API_KEY = process?.env?.CHATGPT_API_KEY;
const configuration = new Configuration({
	apiKey: API_KEY
});
const openai = new OpenAIApi(configuration);

async function fetchDailyAdvice(history) {
	const prompt = `Представь, что ты психолог или персональный коуч. На основе записей дневника сформируй совет пользователю.\n${history}`;
	// const prompt = 'Придумайте вопрос для ведения дневника, который помогает пользователю избегать профессионального выгорания на работе. Действуй в духе психолога и таких дневников, как 6 минут:';

	const response = await openai.createCompletion({
		model: 'text-davinci-003',
		prompt,
		temperature: 0.8,
		max_tokens: 1000,
		top_p: 1,
		frequency_penalty: 0.0,
		presence_penalty: 0.0
	});

	return response.data.choices[0].text.trim();
}

/** @type {import('./$types').RequestHandler} */
export async function POST({ request }) {
	const { history } = await request.json();
	try {
		const result = await fetchDailyAdvice(history);
		return json({ result });
	} catch (err) {
		console.log(err);
		return json({ result: JSON.stringify(err) });
	}
}
