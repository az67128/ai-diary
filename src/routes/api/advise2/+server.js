import { json } from '@sveltejs/kit';
import { Configuration, OpenAIApi } from 'openai';

const API_KEY = process?.env?.CHATGPT_API_KEY;
const configuration = new Configuration({
	apiKey: API_KEY
});
const openai = new OpenAIApi(configuration);

// const content = "Придумай 1 вопрос для ведения дневника, который помогает пользователю избегать профессионального выгорания на работе. Действуй в духе психолога и таких дневников, как 6 минут";

const content = 'Дай пользователю совет на основе его записей дневника, действуй как коуч или психолог';

async function fetchDailyAdvice(history) {
	try {
		const completion = await openai.createChatCompletion({
			model: 'gpt-3.5-turbo',
			messages: [ ...history, { role: 'system', content }],
			temperature: 0.8,
			max_tokens: 500
		});

		return completion.data.choices[0].message;
	} catch (err) {
		console.log(err);
	}
}

/** @type {import('./$types').RequestHandler} */
export async function POST({ request }) {
	const { history } = await request.json();
	try {
		const result = await fetchDailyAdvice(history);
		return json({ result });
	} catch (err) {
		return json({ result: JSON.stringify(err) });
	}
}
