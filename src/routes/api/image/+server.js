import { json } from '@sveltejs/kit';
import { Configuration, OpenAIApi } from "openai";

const API_KEY = process?.env?.CHATGPT_API_KEY;
const configuration = new Configuration({
  apiKey: API_KEY,
});
const openai = new OpenAIApi(configuration);



async function fetchDailyQuestion() {
    // const prompt = 'I want you to act a coach. Provide a question for today\'s journal entry. Language: russian';
    // const prompt = 'Придумайте вопрос для ведения дневника, который помогает пользователю избегать профессионального выгорания на работе. Действуй в духе психолога и таких дневников, как 6 минут:';

    const response = await openai.createImage({
        prompt: "It was a wonderful day on vacation. First we went to the mountain, where we cracked rolls overlooking the mountain, and then we went to Derbent. We walked around the center and went for a delicious dinner. In the evening I completed the prototype of the diary in which I am writing this note. unreal engine",
        n: 2,
        size: "512x512",
      });
    
    console.log(response.data)
    return response.data;
}


/** @type {import('./$types').RequestHandler} */
export async function GET() {
	const result = await fetchDailyQuestion()

	return json({result});
	
}
