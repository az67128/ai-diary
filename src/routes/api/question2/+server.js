import { json } from '@sveltejs/kit';
import { Configuration, OpenAIApi } from "openai";

const API_KEY = process?.env?.CHATGPT_API_KEY;
const configuration = new Configuration({
  apiKey: API_KEY,
});
const openai = new OpenAIApi(configuration);


// const content = "Придумай 1 вопрос для ведения дневника, который помогает пользователю избегать профессионального выгорания на работе. Действуй в духе психолога и таких дневников, как 6 минут";
const content = "Придумай 1 вопрос для ведения дневника";
async function fetchDailyQuestion() {
    const completion = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: [{role: "system", content }],
        temperature: 0.8,
        max_tokens: 100
      });
      console.log(completion.data.choices[0].message);

 
    
    return completion.data.choices[0].message
}


/** @type {import('./$types').RequestHandler} */
export async function GET() {
	const result = await fetchDailyQuestion()

	return json({result});
	
}
